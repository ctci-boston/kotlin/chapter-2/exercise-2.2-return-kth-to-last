import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `When k is 0 and the linked list is empty verify null is returned`() {
        val k = 0
        val list = null
        val actual = kthToLast(k, list)
        val expected: Node? = null
        assertEquals(expected, actual)
    }

    @Test fun `When k is 0 and the linked list has one node verify that node is returned`() {
        val k = 0
        val list = Node(0, null)
        val actual = kthToLast(k, list)
        val expected: Node? = list
        assertEquals(expected, actual)
    }

    @Test fun `When k is 0 and the linked list has many nodes verify the last node is returned`() {
        val k = 0
        val list = createLinkedList(1, 2, 3, 4, 5, 6)
        val actual = kthToLast(k, list)
        val expected: Node? = Node(6)
        assertEquals(expected, actual)
    }

    @Test fun `When k is 1 and the linked list has two nodes verify the head node is returned`() {
        val k = 1
        val list = createLinkedList(1, 2)
        val actual = kthToLast(k, list)
        val expected: Node? = list
        assertEquals(expected, actual)
    }

    @Test fun `When k is 1 and the linked list has more than two nodes verify the next to last node is returned`() {
        val k = 1
        val list = createLinkedList(1, 2, 3, 4, 5, 6)
        val actual = kthToLast(k, list)
        val expected = createLinkedList(5, 6)
        assertEquals(expected, actual)
    }

    @Test fun `When k is more than 1 and the list has more than two nodes verify the kth to last node is returned`() {
        val k = 2
        val list = createLinkedList(1, 2, 3, 4, 5, 6)
        val actual = kthToLast(k, list)
        val expected = createLinkedList(4, 5, 6)
        assertEquals(expected, actual)
    }

    @Test fun `When k is larger than the given list verify that the list is returned`() {
        val k = 21
        val list = createLinkedList(1, 2, 3, 4, 5, 6)
        val actual = kthToLast(k, list)
        val expected = list
        assertEquals(expected, actual)
    }
}
