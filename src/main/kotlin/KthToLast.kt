data class Node(var value: Int, var next: Node? = null)

fun createLinkedList(vararg values: Int): Node? {
    fun appendToList(head: Node) {
        var current = head
        fun addNodeToList(node: Node) {
            current.next = node
            current = node
        }

        for (n in 1 until values.size) addNodeToList(Node(values[n]))
    }
    val head = if (values.isNotEmpty()) Node(values[0]) else null

    if (head != null && values.size > 1) appendToList(head)
    return head
}

fun kthToLast(k: Int, head: Node?): Node? {
    tailrec fun kthToLast(k: Int, result: Node?, current: Node?): Node? {
        fun nextResult(): Node? = if (k <= 0 && current?.next != null) result?.next else result

        if (current == null) return result
        return kthToLast(k - 1, nextResult(), current.next)
    }

    return kthToLast(k, head, head)
}
